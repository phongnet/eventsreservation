(function () {
    'use strict';

    angular
        .module('Petronas.Intranet.Web.User.Documentation')
        .controller('ApiController', ApiController);

    ApiController.$inject = ["API_DATA"];
    function ApiController(API_DATA) {

        var vm = this;

        vm.allPages = API_DATA;

    }

})();
