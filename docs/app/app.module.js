(function () {
    'use strict';

    angular
        .module('Petronas.Intranet.Web.User.Documentation', [
            'ui.router'
        ]);

})();
