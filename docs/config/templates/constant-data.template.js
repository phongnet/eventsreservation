angular
    .module('Petronas.Intranet.Web.User.Documentation')
    .constant('{$ doc.name $}', {$ doc.items | json $});
