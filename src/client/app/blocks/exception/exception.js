(function() {
    'use strict';

    angular
        .module('blocks.exception')
        .factory('exception', exception);

    /* @ngInject */
    function exception(logger) {
        return {
            catcher: function(message) {
                return function(reason) {
                    logger.error(message, reason);
                };
            }
        };
    }
})();
