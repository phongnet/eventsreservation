(function () {
    'use strict';

    angular
        .module('blocks.router')
        .run(/* @ngInject */function ($urlRouter) {
            $urlRouter.listen();

            // FOR GOOGLE ANALYTIC WHEN DEPLOY TO PRODCUTION PLEASE UN-COMMENT THIS BLOCK
            // $rootScope.$on('$viewContentLoaded', function (event) {
            //     $window.ga('send', 'pageview', {
            //         page: $location.url()
            //     });
            // });

        });
})();
