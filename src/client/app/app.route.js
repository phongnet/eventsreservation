(function () {
    'use strict';

    angular
        .module('app')
        .config(routeConfiguration);


    /* @ngInject */
    function routeConfiguration($stateProvider) {
        $stateProvider
            .state('app', {
                abstract: true,
                template: '<div ui-view></div>'
            });
    }

})();
