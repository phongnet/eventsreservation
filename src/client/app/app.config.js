(function() {
    'use strict';

    angular
        .module('app')
        .config(configFn);

    configFn.$inject = ['DEBUG_INFO_ENABLED', '$compileProvider'];
    function configFn(DEBUG_INFO_ENABLED, $compileProvider) {
        // show debug info in development
        $compileProvider.debugInfoEnabled(DEBUG_INFO_ENABLED);

    }

})();
