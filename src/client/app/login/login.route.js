(function () {
    'use strict';

    angular
        .module('app.login')
        .config(routeConfiguration);

    /* @ngInject */
    function routeConfiguration($stateProvider) {

        $stateProvider
            .state('login', {
                parent: 'app',
                url: '/login',
                templateUrl: 'app/login/login.html',
                controller: 'LoginController',
                controllerAs: 'vm',
                title: 'Login',
                id: 'login',
                data: {
                    head: {
                        title: 'GS - Login',
                        keywords: 'GS - Login',
                        description: 'GS - Login'
                    }
                }
                // ,
                // resolve    : {
                //     authenticated: /* @ngInject */function ($session, $q, $state) {
                //
                //         var user     = $session.getUser(); // Empty object {} will returned if user not logged in
                //         var deferred = $q.defer();
                //
                //         if (user && Object.keys(user).length !== 0) {
                //             deferred.reject({
                //                 message: 'Already Authorized'
                //             });
                //         } else {
                //             deferred.resolve();
                //         }
                //
                //         return deferred.promise.catch(function () {
                //             $state.go('myLibrary');
                //         });
                //     }
                // }
            });

    }

})();
