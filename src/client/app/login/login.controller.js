(function() {
    'use strict';

    angular
        .module('app.login')
        .controller('LoginController', LoginController);

    /* @ngInject */
    function LoginController($state) {

        var vm = this;

        vm.login  = login;

        ////////////////////////////////////////////////////////////////////
        ///////////////////////////  METHODS ///////////////////////////////
        ////////////////////////////////////////////////////////////////////

        function login() {
            $state.go('myMemo');
        }

    }
})();
