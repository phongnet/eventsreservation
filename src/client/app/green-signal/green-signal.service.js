(function () {
    'use strict';

    angular
    .module('app.greenSignal')
    .factory('greenSignalService', greenSignalService);

    /* @ngInject */
    function greenSignalService (jQuery) {

        var service = {
            closeModal: _closeModal,
            openModal: _openModal,
            createShortName: _createShortName
        };

        function _openModal(modalSelector) {
            jQuery(modalSelector).modal('show');
        }

        function _createShortName(_firstName, _lastName) {
            var firstShortName = '';
            var lastShortName = '';
            if (_firstName) {
                firstShortName = _firstName.substr(0, 1);
            }
            if (_lastName) {
                lastShortName = _lastName.substr(0, 1);
            }
            var currentUserNameShortcut = firstShortName + lastShortName;
            return currentUserNameShortcut;
        }

        // There is a bug that the modal-backdrop keep open after closing modal
        // Hide modal using `.modal('hide')` over `data-dismiss="modal"`
        // will help to solve the problem
        function _closeModal(modalSelector) {
            jQuery(modalSelector).modal('hide');
        }
        return service;
    }
})();
