(function () {
    'use strict';

    angular
        .module('app.greenSignal')
        .directive('checkbox', checkbox);

    /* @ngInject */
    function checkbox() {
        var directive = {
            bindToController: true,
            controller: CheckboxController,
            controllerAs: 'vm',
            restrict: 'E',
            scope: {
                checked: '=',
                size: '@'
            },
            templateUrl: 'app/green-signal/directives/checkbox/checkbox.html',
            link: function (scope, element, attrs) {
                var vm = scope.vm;

                vm.customStyle = {
                    width: (vm.size ? vm.size : 20) + 'px',
                    height: (vm.size ? vm.size : 20) + 'px'
                };
                vm.iconStyle = {
                    fontSize: (vm.size ? vm.size : 20) + 'px'
                }

                vm.onCheck = _onCheck;
                function _onCheck () {
                    vm.checked = !vm.checked;
                }
            }
        };

        /* @ngInject */
        function CheckboxController() {
            var vm = this;
        }

        return directive;
    }
})();
