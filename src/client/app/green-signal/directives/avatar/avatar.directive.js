(function () {
    'use strict';

    angular
        .module('app.greenSignal')
        .directive('avatar', avatar);

    /* @ngInject */
    function avatar(randomColor) {
        var directive = {
            bindToController: true,
            controller: AvatarController,
            controllerAs: 'vm',
            restrict: 'E',
            scope: {
                fullName: '=',
                imageUrl: '=',
                size: '@',
                fontSize: '@',
                backgroundColor: '@',
                isBorder: '@',
                more: '@'
            },
            templateUrl: 'app/green-signal/directives/avatar/avatar.html',
            link: function (scope, element, attrs) {
                var vm = scope.vm;
                if (vm.more) {
                    vm.shortcutName = '+' + vm.more;
                } else {
                    vm.shortcutName = vm.fullName ? vm.fullName.replace(/[^A-Z]/g, '') : '?';
                }
                if (vm.size) {
                    vm.fontSize = vm.fontSize ? vm.fontSize : (vm.size / 2);
                    vm.customStyle = {
                        borderRadius: (vm.size / 2) + 'px',
                        width: vm.size + 'px',
                        height: vm.size + 'px',
                        lineHeight: vm.size + 'px',
                        fontSize: vm.fontSize + 'px',
                        backgroundColor: vm.backgroundColor ? vm.backgroundColor : randomColor({
                            luminosity: 'dark',
                            format: 'rgb'
                        })
                    };
                }
                if (vm.imageUrl) {
                    vm.customStyle.backgroundImage = 'url(' + vm.imageUrl + ')';
                    vm.customStyle.backgroundSize = 'cover';
                }
                if (vm.isBorder) {
                    vm.customStyle.border = '1px solid #FFF';
                }
            }
        };

        /* @ngInject */
        function AvatarController() {
            var vm = this;
            vm.shortcutName = '';
            vm.customStyle = {};
        }

        return directive;
    }
})();
