(function () {
    'use strict';

    angular
        .module('app.greenSignal')
        .config(greenSignalConfigurtion);

    /* @ngInject */
    function greenSignalConfigurtion($stateProvider) {
        $stateProvider
            .state('greenSignal', {
                url: '/',
                templateUrl: 'app/green-signal/green-signal.html',
                resolve: {
                    authenticated: /* @ngInject */function ($session, $q, $state) {
                        var user = $session.getUser(); // Empty object {} will returned if user not logged in
                        var deferred = $q.defer();
                        
                        // HARD CODE TO GO DASHBOARH WITHOUT LOGIN
                        deferred.resolve();

                        if (user && Object.keys(user).length === 0) {
                            deferred.reject({
                                message: 'Unauthorized'
                            });
                        } else {
                            deferred.resolve();
                        }

                        return deferred.promise
                        .then(function() {
                            // Need to use setTimeout, if not you will
                            // get empty current.name and abstract current state
                            setTimeout(function() {
                                var current = $state.current;

                                // Go to dashboard state for index route
                                if (current.name === 'greenSignal' && current.url === '/') {
                                    $state.go('greenSignal.dashboard');
                                }
                            });
                        })
                        .catch(function () {
                            $state.go('login');
                        });
                    }
                }
            })
            .state('greenSignal.dashboard', {
                parent: 'greenSignal',
                url: '^/dashboard',
                templateUrl: 'app/green-signal/dashboard/dashboard.html',
                onEnter: /* @ngInject */function ($location) {
                    $location.path('/dashboard/inbox');
                }
            });
    }

})();
