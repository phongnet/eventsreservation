(function () {
    'use strict';
  
    angular
      .module('app.greenSignal')
      .controller('GreenSignalController', GreenSignalController);
  
    /* @ngInject */
    function GreenSignalController () {
      var vm = this;
      vm.menu = {
          toogle: false
      };
    }
  })();
  