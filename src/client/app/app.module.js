(function() {

    'use strict';

    /**
     * @ngdoc module
     * @name app
     *
     * @description
     * GS module
     *
     */
    angular.module('app', [
        'app.core',
        'app.login',
        'app.layout',
        'app.greenSignal',
    ]);

})();
