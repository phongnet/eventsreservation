(function () {
    'use strict';

    angular
        .module('app.layout')
        .directive('sideMenuBar', sideMenuBar);

    /* @ngInject */
    function sideMenuBar () {

        return {
            bindToController: true,
            controller      : SideMenuBarController,
            controllerAs    : 'vm',
            restrict        : 'E',
            scope           : {
                menu: '='
            },
            templateUrl     : 'app/layout/side-menu-bar/side-menu-bar.html'
        };

        /* @ngInject */
        function SideMenuBarController ($state, $session) {
            var currentRoute = $state.$current.url.sourcePath.substring(1);
            var vm = this;

            vm.menus = [
                {
                    name: 'dashboard',
                    icon: 'icon-pet-i-tile',
                    isActive: currentRoute === 'dashboard' && true,
                    redirect: '.dashboard',
                    forRole: true
                },
                {
                    name: 'item 2',
                    icon: 'icon-pet-i-i-archive',
                    isActive: false,
                    redirect: '.',
                    forRole: true
                },
                {
                    name: 'item 3',
                    icon: 'icon-pet-i-doc',
                    isActive: false,
                    redirect: '.',
                    forRole: true
                }
            ];
            vm.activeMenu = _activeMenu;

            // METHODS HERE
            function _activeMenu(menuActive) {
                vm.menus.forEach(function (item) {
                    if (item.name === menuActive) {
                        item.isActive = true;
                    }
                    else {
                        item.isActive = false;
                    }
                });
            }
        }
    }
})();
