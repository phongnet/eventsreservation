(function() {
    'use strict';

    angular
        .module('app.layout')
        .directive('footerApp', footerApp);

    /* @ngInject */
    function footerApp() {
        var directive = {
            bindToController: true,
            controller: FooterController,
            controllerAs: 'vm',
            restrict: 'EA',
            scope: {},
            templateUrl: 'app/layout/footer/footer.html'
        };

        /* @ngInject */
        function FooterController() {
            var vm = this;
        }

        return directive;
    }
})();
