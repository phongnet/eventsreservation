(function () {
    'use strict';

    angular
        .module('app.layout')
        .directive('headerApp', headerApp);

    /* @ngInject */
    function headerApp() {

        return {
            bindToController: true,
            controller      : HeaderController,
            controllerAs    : 'vm',
            restrict        : 'E',
            scope           : {
                appName: '@',
                menu: '='
            },
            templateUrl     : 'app/layout/header/header.html'
        };

        /* @ngInject */
        function HeaderController($rootScope, $session, $state, greenSignalService) {
            var vm = this;
            vm.IsMobileMenuActive = false;
            vm.toogleSideMenu = _toogleSideMenu;
            vm.toogleImgModal = _toogleImgModal;
            vm.userView = false;
            vm.getUserInfo = _getUserInfo;
            vm.logOutCurrentUser = _logOutCurrentUser;
            vm.currentUserNameShortcut = null;
            vm.avatarMode = 'image';
            vm.userInfo = [];
            _getUserInfo();


            vm.topMenuRight = [
                {
                    name: 'search',
                    icon: 'icon-pet-i-search',
                    isActive: true
                },

                {
                    name: 'notification',
                    icon: 'icon-pet-i-i-bell',
                    isActive: true
                }
            ];

            // METHODS
            function _toogleSideMenu () {
                vm.menu.toogle = !vm.menu.toogle;
            }

            function _toogleImgModal($event) {
                vm.userView = !vm.userView;
                $event.stopPropagation();
            }
            
            window.onclick = function ($event) {
                var target = $event.target;
                if (!target.closest('#user-profile')) {
                    vm.userView = false;
                    $rootScope.$apply();
                }
            };

            function _getUserInfo () {
                vm.userInfo = $session.getUser();
                if (!vm.userInfo.photo) {
                    vm.avatarMode = 'name';
                    vm.currentUserNameShortcut = greenSignalService.createShortName(vm.userInfo.firstName, vm.userInfo.lastName);
                }
            }

            function _logOutCurrentUser () {
                $session.LogOut();
                $state.go('login');
            }
        }
    }
})();
