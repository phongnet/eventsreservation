(function () {
    'use strict';

    var core = angular.module('app.core');

    var config = {
        appErrorPrefix: '[Inttranet Error] ', //Configure the exceptionHandler decorator
        appTitle: 'Intranet User Portal',
        imageBasePath: '/images/'
    };

    core.value('config', config);

    core.config(configure);

    /* @ngInject */
    function configure($logProvider, exceptionHandlerProvider) {

        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        configureStateHelper();

        ////////////////

        function configureStateHelper() {
            var resolveAlways = {
                ready: ready
            };

            /* @ngInject */
            function ready(dataservice) {
                return dataservice.ready();
            }

        }
    }
})();
