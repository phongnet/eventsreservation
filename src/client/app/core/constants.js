/* global toastr:false, moment:false */
(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('moment', moment)
        .constant('appConfig', {
            // apiUrl: 'http:#####/',
            passwordKey: 'CC3D6611-4B5E-45DE-9CA3-FE9124AB6D00',
            baseHref: '/',
        });
})();
