(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('CryptoJS', /* @ngInject */function ($window) {
            return $window.CryptoJS;
        });

})();
