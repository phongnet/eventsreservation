(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('common', common)
        .factory('jQuery', /* @ngInject */function($window) {
            return $window.jQuery;
        })
        .factory('randomColor', /* @ngInject */function($window) {
            return $window.randomColor;
        });


    /* @ngInject */
    function common(jQuery, $window) {
        var service = {
            showLoadingBig: showLoadingBig,
            showLoadingSmall: showLoadingSmall,
            hideLoading: hideLoading,
            historyBack: historyBack,
            showPageLoading: showPageLoading,
            hidePageLoading: hidePageLoading
        };

        return service;
        // var $ = jQuery;


        function showPageLoading() {
            jQuery('#page-loading-ind').show();
        }

        function hidePageLoading() {
            jQuery('#page-loading-ind').slideUp();
        }

        function showLoadingBig(parent) {
            hideLoading(parent);
            jQuery(parent).append('<div class="div-loading"><div class="uil-spin-css loading-icon big-loading" style="-webkit-transform:scale(0.24)"><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div></div>');
        }

        function showLoadingSmall(parent) {
            jQuery(parent).append('<div class="loading loading-small"></div>');
        }

        function hideLoading(parent) {
            jQuery(parent + ' .div-loading').remove();
        }

        function historyBack() {
            $window.history.back();
        }
    }
})();
