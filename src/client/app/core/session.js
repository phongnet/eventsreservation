(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('$session', /* @ngInject */function ($localstorage, appConfig) {
            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + 365);
            var passwordKey = appConfig.passwordKey;
            return {
                LogOut           : function () {
                    var isFirstVisit = $localstorage.get('intranet-isFirstVisit');
                    if (isFirstVisit) {
                        $localstorage.remove('intranet-isFirstVisit');
                    }


                    $localstorage.remove('intranet-gateway');
                    $localstorage.remove('intranet-userPreference');
                    $localstorage.remove('intranet-lastUpdateMenu');
                    $localstorage.remove('intranet-HeaderData');
                    $localstorage.remove('intranet-MenuData');
                    $localstorage.remove('intranet-FooterData');
                    $localstorage.remove('intranet-isUpdated');
                    $localstorage.remove('intranet-OPU');
                    $localstorage.remove('intranet-returnUrl');
                    $localstorage.remove('intranet-role');
                    $localstorage.remove('intranet-accesstoken');
                    $localstorage.remove('intranet-user');
                },
                // setToken         : function (token) {
                //     var accessToken = CryptoJS.AES.encrypt(token, passwordKey).toString();
                //     $localstorage.set('intranet-accesstoken', accessToken);
                // },
                // getToken         : function () {
                //     var token = $localstorage.get('intranet-accesstoken');
                //     if (token === null || token === undefined) {
                //         return undefined;
                //     }
                //     return CryptoJS.AES.decrypt(token, passwordKey).toString(CryptoJS.enc.Utf8);
                // },
                setToken: function(token){
                    $localstorage.set('accesstoken', token);
                },
                getToken: function(){
                    return $localstorage.get('accesstoken');
                },
                setIsFirstLogin  : function (isfirstLogin) {
                    $localstorage.set('intranet-IsFirstLogin', isfirstLogin);
                },
                getIsFirstLogin  : function () {
                    return $localstorage.get('intranet-IsFirstLogin');
                },
                setUser          : function (user) {
                    if (user) {
                        if ((user.firstName === null || user.firstName === '') && (user.lastName === null || user.lastName === '')) {
                            var userName = user.userId.split('\\');
                            if (userName.length > 1) {
                                user.firstName = userName[1];
                            } else {
                                user.firstName = user.userId;
                            }

                        }

                        $localstorage.setObject('intranet-user', user);
                    }

                },
                getUser          : function () {

                    var user = $localstorage.getObject('intranet-user');

                    if (user && user.Firstname && user.Lastname) {
                        user.fullName = user.Firstname + ' ' + user.Lastname;
                    }
                    else if (user.Username) {
                        user.fullName = user.Username.split('\\')[1];
                    }

                    return user;
                    // return $localstorage.getObject('intranet-user');
                },
                setGateway       : function (gateway) {
                    $localstorage.setObject('intranet-gateway', gateway);
                },
                getGateway       : function () {
                    return $localstorage.getObject('intranet-gateway');
                },
                setLastUpdateMenu: function (gateway) {
                    $localstorage.setObject('intranet-lastUpdateMenu', gateway);
                },
                getLastUpdateMenu: function () {
                    return $localstorage.getObject('intranet-lastUpdateMenu');
                },
                setHeaderData    : function (HeaderData) {
                    $localstorage.setObject('intranet-HeaderData', HeaderData);
                },
                getHeaderData    : function () {
                    return $localstorage.get('intranet-HeaderData');
                },
                setMenuData      : function (MenuData) {
                    $localstorage.setObject('intranet-MenuData', MenuData);
                },
                getMenuData      : function () {
                    return $localstorage.get('intranet-MenuData');
                },
                setFooterData    : function (FooterData) {
                    $localstorage.setObject('intranet-FooterData', FooterData);
                },
                getFooterData    : function () {
                    return $localstorage.get('intranet-FooterData');
                },
                setIsUpdated     : function (isUpdated) {
                    $localstorage.set('intranet-isUpdated', isUpdated);
                },
                getIsUpdated     : function () {
                    return $localstorage.get('intranet-isUpdated');
                },
                setOPU           : function (OPU) {
                    $localstorage.setObject('intranet-OPU', OPU);
                },
                getOPU           : function () {
                    return $localstorage.getObject('intranet-OPU');
                },
                setReturnUrl     : function (url) {
                    $localstorage.set('intranet-returnUrl', url);
                },
                getReturnUrl     : function () {
                    return $localstorage.get('intranet-returnUrl');
                },

                setIsFirstVisit  : function (val) {
                    $localstorage.set('intranet-isFirstVisit', val);
                },
                getIsFirstVisit  : function () {
                    return $localstorage.get('intranet-isFirstVisit');
                },
                setUserPreference: function (userPref) {
                    $localstorage.setObject('intranet-userPreference', userPref);
                },
                getUserPreference: function () {
                    return $localstorage.getObject('intranet-userPreference');
                },
                setRole          : function (role) {
                    $localstorage.setObject('intranet-role', role);
                },
                getRole          : function () {
                    return $localstorage.getObject('intranet-role');
                },
                clear            : function () {
                    var isFirstVisit = $localstorage.get('intranet-isFirstVisit');
                    if (isFirstVisit) {
                        $localstorage.remove('intranet-isFirstVisit');
                    }


                    $localstorage.remove('intranet-gateway');
                    $localstorage.remove('intranet-userPreference');
                    $localstorage.remove('intranet-lastUpdateMenu');
                    $localstorage.remove('intranet-HeaderData');
                    $localstorage.remove('intranet-MenuData');
                    $localstorage.remove('intranet-FooterData');
                    $localstorage.remove('intranet-isUpdated');
                    $localstorage.remove('intranet-OPU');
                    $localstorage.remove('intranet-returnUrl');
                    $localstorage.remove('intranet-role');
                    $localstorage.remove('intranet-accesstoken');
                    $localstorage.remove('intranet-user');
                }
            };
        });

})();

