(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('$localstorage', localstorage);

    /* @ngInject */
    function localstorage($window) {
        var service = {
            set: set,
            get: get,
            setObject: setObject,
            getObject: getObject,
            clear: clear,
            remove: remove
        };

        return service;

        function set(key, value) {
            $window.localStorage[key] = value;
        }

        function get(key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        }

        function setObject(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        }

        function getObject(key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }

        function clear() {
            $window.localStorage.clear();
        }

        function remove(key) {
            $window.localStorage.removeItem(key);
        }
    }
})();
